<?php
/**
 * @file plugins.inc
 * Built in plugins for Views output handling.
 *
 */

/**
 * Implementation of hook_views_plugins
 */
function views_rss_simple_views_plugins() {
  return array(
    'style' => array(
      'rss_custom' => array(
        'title' => t('RSS Simple Feed - no query string'),
        'help' => t('Generates an RSS custom feed from a view with no query string in the URL.'),
        'handler' => 'views_rss_simple_plugin_style',
        'theme' => 'views_view_rss',
        'uses row plugin' => TRUE,
        'uses options' => TRUE,
        'type' => 'feed',
        'help topic' => 'style-rss',
        'parent' => 'rss',
      ),
    ),
  );
}
