<?php
/**
 * @file
 * Contains the default style plugin.
 */

/**
 * Default style plugin to render rows one after another with no
 * decorations.
 *
 * @ingroup views_style_plugins
 */
class views_rss_simple_plugin_style extends views_plugin_style_rss {
  function attach_to($display_id, $path, $title) {
    $display = $this->view->display[$display_id]->handler;
    if ($this->options['exclude_args']) {
      $url = url($path);
    }
    else {
      $url = url($this->view->get_url(NULL, $path));
    }

    if ($display->has_path()) {
      if (empty($this->preview)) {
        drupal_add_feed($url, $title);
      }
    }
    else {
      if (empty($this->view->feed_icon)) {
        $this->view->feed_icon = '';
      }
      $this->view->feed_icon .= theme('feed_icon', $url, $title);
      drupal_add_link(array(
        'rel' => 'alternate',
        'type' => 'application/rss+xml',
        'title' => $title,
        'href' => $url
      ));
    }
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['exclude_args'] = array('default' => '', 'translatable' => TRUE);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['exclude_args'] = array(
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['exclude_args']),
      '#title' => t('Omit arguments from the display that attaches this RSS feed.'),
      '#description' => t('This will make the RSS even more simple.'),
    );
  }

}
